package projecteJoel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Classe de proves JUnit per a la classe GenshinImpact.
 */
class GenshinImpactTest {

	GenshinImpact gen;
	/**
     * Configura l'objecte GenshinImpact abans de cada execució del mètode de prova.
     */
	@BeforeEach
	void setUp() {
		gen = new GenshinImpact();
	}
	/**
     * Cas de prova per verificar el temps necessari quan totes les habilitats estan al nivell mínim i no hi ha materials disponibles.
     */
	@Test
	void testMin() {
		int temps = gen.calcularTemps(1, 1, 1, 0);
		assertEquals(17, temps);
	}
	/**
     * Cas de prova per verificar el temps necessari quan totes les habilitats estan en un nivell mínim i hi ha prou material disponible.
     */
	@Test
	void testMin2() {
		int temps = gen.calcularTemps(1, 1, 1, 1224);
		assertEquals(0, temps);
	}
	/**
     * Cas de prova per verificar el temps necessari quan totes les habilitats estan al nivell mínim i hi ha una quantitat molt gran de materials disponibles.
     */
	@Test
	void testMin3() {
		int temps = gen.calcularTemps(1, 1, 1, 10000);
		assertEquals(0, temps);
	}
	/**
     * Cas de prova per verificar el temps necessari quan totes les habilitats estan al nivell mínim i hi ha una quantitat moderada de materials disponibles.
     */
	@Test
	void testMin4() {
		int temps = gen.calcularTemps(1, 1, 1, 666);
		assertEquals(8, temps);
	}
	/**
     * Cas de prova per verificar el temps necessari quan totes les habilitats estan al nivell màxim i no hi ha materials disponibles.
     */
	@Test
	void testMax1() {
		int temps = gen.calcularTemps(10, 10, 10, 0);
		assertEquals(0, temps);
	}
	/**
     * Cas de prova per verificar el temps necessari quan totes les habilitats estan al nivell màxim i hi ha una petita quantitat de materials disponibles.
     */
	@Test
	void testMax2() {
		int temps = gen.calcularTemps(10, 10, 10, 72);
		assertEquals(0, temps);
	}
	/**
     * Cas de prova per verificar el temps necessari per a pujar de nivell les habilitats quan s'ofereixen diferents nivells i quantitats de materials.
     */
	@Test
	void test() {
		int temps = gen.calcularTemps(3, 5, 7, 9);
		assertEquals(15, temps);
	}

}