package projecteJoel;

import java.util.Scanner;
/**
 * Aquesta classe representa un programa senzill per calcular el temps necessari per a pujar de nivell les habilitats d'un personatge en el joc "Genshin Impact".
 */
public class GenshinImpact {
	static Scanner sc = new Scanner(System.in);
	/**
     * Mètode principal per obtenir les dades de l'usuari i calcular el temps necessari.
     * @param args Arguments de línia de comandes
     */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int nivellA = getValidLevel("A");
        int nivellE = getValidLevel("E");
        int nivellQ = getValidLevel("Q");
        int materialsVerds = getValidMaterials("materials");
        
		int tempsNecessari = calcularTemps(nivellA, nivellE, nivellQ, materialsVerds);
		System.out.println(tempsNecessari);
	}
	/**
     * Obté un nivell de habilitat vàlid de l'usuari.
     *
     * @param nivell Identificador de la habilitat (A, E o Q)
     * @return Nivell de la habilitat
     * @throws IllegalArgumentException Si el nivell de la habilitat no està dins del rang permès (1 a 10)
     */
	public static int getValidLevel(String nivell) {
        int level = sc.nextInt();
        if (level < 1 || level > 10) {
            throw new IllegalArgumentException("Error: El nivell de la habilitat " + nivell + " ha d'estar entre 1 i 10");
        }
        return level;
    }
	/**
     * Obté una quantitat de material vàlida de l'usuari.
     *
     * @param m Identificador del material
     * @return Quantitat de material
     * @throws IllegalArgumentException Si la quantitat de material és negativa
     */
	public static int getValidMaterials(String m) {
        int materials = sc.nextInt();
        if (materials < 0) {
            throw new IllegalArgumentException("Error: La quantitat de " + m + " ha de ser positiva.");
        }
        return materials;
    }
	/**
     * Calcula el temps necessari per pujar de nivell les habilitats.
     *
     * @param nivellA Nivell de l'habilitat A
     * @param nivellE Nivell de l'habilitat E
     * @param nivellQ Nivell de l'habilitat Q
     * @param materialsVerds Quantitat de materials existents (convertit a verds)
     * @return Temps necessari en dies
     */
	public static int calcularTemps(int nivellA, int nivellE, int nivellQ, int materialsVerds) {
		int materialsA = calcularMaterials(nivellA);
		int materialsE = calcularMaterials(nivellE);
		int materialsQ = calcularMaterials(nivellQ);

		int totalMaterials = materialsA + materialsE + materialsQ - materialsVerds;
		totalMaterials = Math.max(totalMaterials, 0);
		int diesNecessaris = totalMaterials / 72;
		if (totalMaterials % 72 != 0) {
			diesNecessaris++;
		}
		return diesNecessaris;
	}
	/**
     * Calcula la quantitat total de material necessària per pujar de nivell l'habilitat especificada.
     *
     * @param nivell Nivell de l'habilitat
     * @return Quantitat total de material necessària
     */
	public static int calcularMaterials(int nivell) {
		int[] materialsNivell = { 3, 6, 12, 18, 27, 36, 54, 108, 144, 0 };
		
		int totalMaterials = 0;
		
		for (int i = nivell - 1; i < 10; i++) {
			totalMaterials += materialsNivell[i];
		}
		return totalMaterials;
	}

}